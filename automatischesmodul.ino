/********************
 * Automatisiertes Mini-Layout mit einer Weiche
 * Steuerung 0.1
 * 
 * Changelog
 * 05.11.2020 - Initiale Erstellung
 * 
 */

// ESP8266 Wifi stuff
#if defined(ESP8266)
#include <ESP8266WiFi.h>          //https://github.com/esp8266/Arduino
#else
#include <WiFi.h>
#endif

// Needed for OTA
#include <ESP8266mDNS.h>
#include <WiFiClient.h>

// WebServer and WifiManager
// WiFi Manager allows easy configuration of the ESPs WiFi, it opens a hotspot with captive portal if no WiFi can be found. 
#include <ESPAsyncWebServer.h>
#include <ESPAsyncWiFiManager.h>         //https://github.com/alanswx/ESPAsyncWiFiManager

// OTA (over the air) Update function
#include <WiFiUdp.h>
#include <ArduinoOTA.h>
#include <Hash.h>

// I/O Functions
//#include <Wire.h>
// PCF8574 I/O expansion board
//#include <pcf8574_esp.h>
// PCA9685 PWM expansion board
//#include <Adafruit_PWMServoDriver.h>

//for LED status
#include <Ticker.h>
Ticker ticker;

// Filesystem for Webserver
//#include <FS.h>

// Pins used: SDA/SCL are i2c, INT is connected to INT pin on pcf board
//#define PIN_INT D5
//#define PIN_SDA D1
//#define PIN_SCL D2

// Initial pins for trying out L298
#define PIN_OUT1 13 // D7
#define PIN_OUT2 15 // D8

// Define Webserver
AsyncWebServer server(80); // for Captive Portal, is deactivated after WiFi connect
//AsyncWebServer webserver(80); // for Control
DNSServer dns;

// WiFi Manager Stuff
void tick()
{
  //toggle state
  int state = digitalRead(LED_BUILTIN);  // get the current state of GPIO1 pin
  digitalWrite(LED_BUILTIN, !state);     // set pin to the opposite state
}

//gets called when WiFiManager enters configuration mode
void configModeCallback (AsyncWiFiManager *myWiFiManager) {
  Serial.println("Entered config mode");
  Serial.println(WiFi.softAPIP());
  //if you used auto generated SSID, print it
  Serial.println(myWiFiManager->getConfigPortalSSID());
  //entered config mode, make led toggle faster
  ticker.attach(0.2, tick);
}

volatile bool PCFInterruptFlag = false;

void ICACHE_RAM_ATTR PCFInterrupt() {
  PCFInterruptFlag = true;
}
// END Wifi Manager Stuff

// Webserver Stuff
// Processor for Webserver
//String processor(const String& var){
//  
//}
// END Webserver Stuff


/* EISENBAHN FUNKTIONEN */

unsigned long previousMillis = 0;
const long interval = 30000; // 30 sec for direction change
bool richtung = true;
int speed = 350; // Range 0 to 1023, 350 seems to be a good value for slow movement with 12V input

void drive(bool direction) {
  if (direction == true) {
    analogWrite(PIN_OUT1, speed);
    analogWrite(PIN_OUT2, 0); 
    digitalWrite(LED_BUILTIN, true);
   } else {
     analogWrite(PIN_OUT1, 0);
     analogWrite(PIN_OUT2, speed); 
     digitalWrite(LED_BUILTIN, false); 
   }
}



void setup() {
  // put your setup code here, to run once:
// put your setup code here, to run once:
  Serial.begin(115200);

  //set led pin as output
  pinMode(LED_BUILTIN, OUTPUT);
  pinMode(PIN_OUT1, OUTPUT);
  pinMode(PIN_OUT2, OUTPUT);
  // start ticker with 0.5 because we start in AP mode and try to connect
  ticker.attach(0.6, tick);

  //WiFiManager
  //Local intialization. Once its business is done, there is no need to keep it around
  AsyncWiFiManager wifiManager(&server,&dns);
  //reset settings - for testing
  //wifiManager.resetSettings();

  //set callback that gets called when connecting to previous WiFi fails, and enters Access Point mode
  wifiManager.setAPCallback(configModeCallback);

  //fetches ssid and pass and tries to connect
  //if it does not connect it starts an access point with the specified name
  //here  "AutoConnectAP"
  //and goes into a blocking loop awaiting configuration
  if (!wifiManager.autoConnect()) {
    Serial.println("failed to connect and hit timeout");
    //reset and try again, or maybe put it to deep sleep
    ESP.reset();
    delay(1000);
  }

  //if you get here you have connected to the WiFi
  Serial.println("connected...yeey :)");
  ticker.detach();
  //keep LED on
  digitalWrite(LED_BUILTIN, LOW);

   // Set Hostname for OTA Updates to identify ESP
  ArduinoOTA.setHostname("Waldbahn");
  
  // OTA Routines
  ArduinoOTA.onStart([]() {
    String type;
    if (ArduinoOTA.getCommand() == U_FLASH) {
      type = "sketch";
    } else { // U_SPIFFS
      type = "filesystem";
    }

    // NOTE: if updating SPIFFS this would be the place to unmount SPIFFS using SPIFFS.end()
    Serial.println("Start updating " + type);
  });
  ArduinoOTA.onEnd([]() {
    Serial.println("\nEnd");
  });
  ArduinoOTA.onProgress([](unsigned int progress, unsigned int total) {
    Serial.printf("Progress: %u%%\r", (progress / (total / 100)));
  });
  ArduinoOTA.onError([](ota_error_t error) {
    Serial.printf("Error[%u]: ", error);
    if (error == OTA_AUTH_ERROR) {
      Serial.println("Auth Failed");
    } else if (error == OTA_BEGIN_ERROR) {
      Serial.println("Begin Failed");
    } else if (error == OTA_CONNECT_ERROR) {
      Serial.println("Connect Failed");
    } else if (error == OTA_RECEIVE_ERROR) {
      Serial.println("Receive Failed");
    } else if (error == OTA_END_ERROR) {
      Serial.println("End Failed");
    }
  });

  ArduinoOTA.begin();
  
  // Debug: send that we are ready and listening on an IP...
  Serial.println("Ready");
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());


  // Initialize SPIFFS
  //if(!SPIFFS.begin()){
  //  Serial.println("An Error has occurred while mounting SPIFFS");
  //  return;
  //}
  previousMillis = millis();
}

void loop() {
  // Handle any OTA request happening
  ArduinoOTA.handle();
  // put your main code here, to run repeatedly:
  if(millis() - previousMillis > interval) {
     richtung = !richtung; // invert direction   
     drive(richtung);
     previousMillis = millis();
  }
     
}
